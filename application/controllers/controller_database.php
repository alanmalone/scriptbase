<?php
/**
* DataBase Class
*/
class Controller_DataBase extends Controller {
	
	function __construct() {
		$this->model = new Model_DataBase();
		$this->view = new View();
	}

	function action_index() {
		session_start();
		if ($_SESSION['admin'] == "union2gb") {
			$data = $this->model->get_data();
			$this->view->generate('database_view.php', 'template_view.php', $data);
		} else {
			session_destroy();
			Route::ErrorPage404();
		}
	}

	function action_logout() {
		session_start();
		session_destroy();
		header('Location:/');
	}
}